function round(num) {
    return Math.round((num + Number.EPSILON) * 100) / 100;
}

function footsToMeters(ft) {
    return round(parseInt(ft) * 0.3);
}

function milesToKm(mi) {
    return round(parseInt(mi) * 1.5);
}

function convertEnabled() {
    return game.settings.get("lang-de-pf2e", "convert");
}

function Right(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else {
        var iLen = String(str).length;
        return String(str).substring(iLen, iLen - n);
    }
}

function ConvertFoots(foot) {
    if (foot) {
        var value = parseInt(foot);
        if (value) {
            if (Right(foot, 5) === 'miles') {
                return value + ' Meilen';
            } else {
                return value + ' Fuß';
            }
        } else {
            return foot;
        }
    }
}

const translatedTime = new Collection([
    ["reaction", "Reaktion"],
    ["Reaction", "Reaktion"],
    ["day", "Tag"],
    ["days", "Tage"],
    ["week", "Woche"],
    ["weeks", "Wochen"],
    ["month", "Monat"],
    ["months", "Monate"],
    ["year", "Jahr"],
    ["hour", "Stunde"],
    ["hours", "Stunden"],
    ["minute", "Minute"],
    ["minutes", "Minuten"],
    ["minute (or longer)", "Minute (oder länger)"],
    ["round", "Runde"],
    ["rounds", "Runden"],
    ["or 2", "or 2"],
    ["to 3", "to 3"],
    ["varies", "variiert"],
    ["see text", "Siehe Text"],
    ["sustained", "Aufrechterhaltbar"],
    ["sustained up to 1 minute", "Bis zu 1 Minute lang aufrechterhalten"],
    ["sustained up to 10 minutes", "Bis zu 10 Minuten lang aufrechterhalten"],
    ["up to 24 hours", "bis zu 24 Stunden"],
    ["round (see text)", "Runde (siehe Text)"],
    ["up to 1 minute", "bis zu 1 Minute"],
    ["See description", "Siehe Beschreibung"],
    ["until the end of your next turn", "bis zum Ende deiner nächsten Runde"],
    ["until the end of the target's next turn", "bis zum Ende des nächsten Zuges des Zieles"],
    ["until the start of your next turn", "bis zum Beginn deiner nächsten Runde"],
    ["until your next daily preparations", "bis zu deinen nächsten täglichen Vorbereitungen"],
    ["unlimited", "Unbegrenzt"]
]);

function ConvertTime(time) {
    if (time) {
        var value = parseInt(time);
        if (value) {
            var start = String(time).search(' ') + 1;
            if (start > 0) {
                var einheit = String(time).substring(start);
                var tr_einheit = translatedTime.get(einheit);
                return value + ' ' + tr_einheit;
            } else {
                return time;
            }
        } else {
            var tr = translatedTime.get(String(time).trim().toLowerCase());
            if (tr === undefined) {
                return time;
            } else {
                return tr;
            }
        }
    }
}

const translatedConditions = new Collection([
    ["Blinded", "Blind/Blinded"],
    ["Broken", "Beschädigt/Broken"],
    ["Clumsy", "Unbeholfen/Clumsy"],
    ["Concealed", "Verborgen/Concealed"],
    ["Confused", "Verwirrt/Confused"],
    ["Controlled", "Gesteuert/Controlled"],
    ["Dazzled", "Geblendet/Dazzled"],
    ["Deafened", "Taub/Deafened"],
    ["Doomed", "Todgeweiht/Doomed"],
    ["Drained", "Ausgelaugt/Drained"],
    ["Dying", "Sterbend/Dying"],
    ["Encumbered", "Belastet/Encumbered"],
    ["Enfeebled", "Kraftlos/Enfeebled"],
    ["Fascinated", "Fasziniert/Fascinated"],
    ["Fatigued", "Erschöpft/Fatigued"],
    ["Flat-Footed", "Auf dem falschen Fuß/Flat-Footed"],
    ["Fleeing", "Fliehend/Fleeing"],
    ["Friendly", "Freundlich/Friendly"],
    ["Frightened", "Verängstigt/Frightened"],
    ["Grabbed", "Gegriffen/Grabbed"],
    ["Helpful", "Hilfsbereit/Helpful"],
    ["Hidden", "Versteckt/Hidden"],
    ["Hostile", "Feindselig/Hostile"],
    ["Immobilized", "Bewegungsunfähig/Immobilized"],
    ["Indifferent", "Gleichgültig/Indifferent"],
    ["Invisible", "Unsichtbar/Invisible"],
    ["Observed", "Beobachtet/Observed"],
    ["Paralyzed", "Gelähmt/Paralyzed"],
    ["Persistent Damage", "Anhaltender Schaden/Persistent Damage"],
    ["Petrified", "Versteinert/Petrified"],
    ["Prone", "Liegend/Prone"],
    ["Quickened", "Beschleunigt/Quickened"],
    ["Restrained", "Gebunden/Restrained"],
    ["Sickened", "Kränkelnd/Sickened"],
    ["Slowed", "Verlangsamt/Slowed"],
    ["Stunned", "Betäubt/Stunned"],
    ["Stupefied", "Benommen/Stupefied"],
    ["Unconscious", "Bewusstlos/Unconscious"],
    ["Undetected", "Unentdeckt/Undetected"],
    ["Unfriendly", "Unfreundlich/Unfriendly"],
    ["Unnoticed", "Unbemerkt/Unnoticed"],
    ["Wounded", "Verwundet/Wounded"],
    ["Dead", "Tot/Dead"]
]);

const translate = function(originalCondition) {
    let translatedCondition = translatedConditions.get(originalCondition);
    return translatedCondition ? translatedCondition : originalCondition;
};


Hooks.once('init', () => {

    if (typeof Babele !== 'undefined') {

        game.settings.register("lang-de-pf2e", "convert", {
            name: "Automatische Konvertierung",
            hint: "Automatische Konvertierung vom britischen in das metrische System.",
            scope: "world",
            type: Boolean,
            default: true,
            config: true
                //            onChange: convert => {
                //                setUnitLabels();
                //                setEncumbranceData();
                //            }
        });

        Babele.get().register({
            module: 'lang-de-pf2e',
            lang: 'de',
            dir: 'compendium'
        });

        Babele.get().registerConverters({
            "coins": (value) => {
                if (!convertEnabled()) {
                    return value;
                }
                if (value) {
                    const tr = { pp: "PM", gp: "GM", sp: "SM", cp: "KM" };
                    return value.toString().replace(/(pp|gp|sp|cp)/gi, matched => tr[matched]);
                }
            },
            "range": (range) => {
                if (!convertEnabled()) {
                    return range;
                }
                if (range) {
                    if (range === 'touch') {
                        return "Berührung";
                    } else if (range === 'planetary') {
                        return "Planetenweit";
                    } else if (range === 'unlimited') {
                        return "Unbegrenzt";
                    } else if (range === 'see text') {
                        return "Siehe Text";
                    } else if (range === 'varies') {
                        return "Variiert";
                    } else {
                        return ConvertFoots(range);
                    }
                } else {
                    return range;
                }
            },
            "time": (time) => {
                if (!convertEnabled()) {
                    return time;
                }
                if (time) {
                    return ConvertTime(time);
                } else {
                    return time;
                }
            },
            "speed": (speed) => {
                const speedTypes = {
                    climb: "Klettern",
                    fly: "Fliegen",
                    swim: "Schwimmen",
                    burrow: "Graben"
                }
                if (!convertEnabled()) {
                    return speed;
                }
                if (speed.value) {
                    speed.value = ConvertFoots(speed.value);
                }
                if (Array.isArray(speed.otherSpeeds)) {
                    var speeds = [];
                    speed.otherSpeeds.forEach((element, index) => {
                        speeds[index] = element;
                        if (element.value) {
                            speeds[index].value = ConvertFoots(element.value);
                        }
                        //                        if (element.type) {
                        //                          result.otherSpeeds[index].type = element.type;
                        //                            var type = element.type.toLowerCase()
                        //                            if (type in speedTypes) {
                        //                                result.otherSpeeds[index].type = speedTypes[type];
                        //                            } else {
                        //                                result.otherSpeeds[index].type = element.type;
                        //                            }
                        //                        }
                    });
                    speed.otherSpeeds = speeds;
                }
                return speed;
            },
            "components": (components) => {
                if (!convertEnabled()) {
                    return components;
                }
                if (components) {
                    const tr = { material: "Material", somatic: "Gesten", verbal: "Verbal" };
                    return components.toString().replace(/(material|somatic|verbal)/gi, matched => tr[matched]);
                }
            },
            "conditionArray": (values) => {
                return values ? values.map(value => { return { "condition": translate(value.condition) }; }) : null;
            },
            "npc-portrait-path": (data, translations, dataObject, translatedCompendium, translationObject) => {
                return game.npcTrans.portrait(data, translations, dataObject, translatedCompendium, translationObject);
            },
            "npc-token-translation": (data, translations, dataObject, translatedCompendium, translationObject) => {
                return game.npcTrans.token(data, translations, dataObject, translatedCompendium, translationObject);
            },
            "npc-data-translation": (data, translations, dataObject, translatedCompendium, translationObject) => {
                return game.npcTrans.data(data, translations, dataObject, translatedCompendium, translationObject);
            },
            "npc-item-translation": (data, translations, dataObject, translatedCompendium, translationObject) => {
                return game.npcTrans.item(data, translations, dataObject, translatedCompendium, translationObject);
            },
        });
    }
});

Hooks.once("babele.ready", () => {
    game.pf2e.ConditionManager.initialize();
});
