﻿$FoundryPath="E:\FoundryVTT"
$DbFile = "E:\FoundryVTT\Data\systems\pf2e\packs\ancestryfeatures.db"
$JsonFile = $env:TEMP+"\pf2e.ancestryfeatures.json"
$Label = "Ancestry Features"

$mappings = @{
    default = [Ordered]@{
        name = "name"
        description = "data.description.value"
    }
    feat = [Ordered]@{
      name = "name"
      description = "data.description.value"
      prerequisites = "data.prerequisites.value"
    }
}


$json = Get-Content -Path $DbFile -Encoding UTF8 | ConvertFrom-Json

if ($json.Count -gt 0) {
    $type = $json[0].type
    if ($mappings.ContainsKey($type)) {
        $mapping = $mappings[$type]
    } else {
        $mapping = $mappings["default"]
    }

    $OutJson = [Ordered]@{
        label = $Label
        mapping = $mapping
        entries = @{}
    }

    ForEach($item in $json) {
        $name = $item.name
        $Entry = [Ordered]@{
            name = $name
            description = $item.data.description.value
            }
        $mapping.GetEnumerator() | ForEach-Object{
            if ($_.Name -ne "name" -and $_.Name -ne "description") {
                $key = $_.Name
                $value = $_.Value
                $mapTable = $_.Value.Split('.')
                $itemValue= $item
                foreach ($nested in $mapTable ) {
                    $itemValue = $itemValue.$nested;
                }
                $Entry.Add($_.Name, $itemValue)
            }
        }
        $OutJson.entries.Add($name, $Entry)
    }
    " "    |  Out-File  -encoding ASCII  -noNewline $JsonFile
    ($OutJson | ConvertTo-Json -Depth 99).Replace('\\n', '\n').Replace("\u003c", '<').Replace("\u003e", '>').Replace("\u0027", "'") | Out-File -Encoding utf8 -append $JsonFile
}    