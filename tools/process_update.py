import subprocess
foundry = 'C:\\Users\\Manfred\\AppData\\Local\\FoundryVTT\\'

compendiums = {
    'actions': {
        'json':         'pf2e.actionspf2e',
        'EN_Label':     'Actions',
        'DE_Label':     'Aktionen'
    },
    'ancestries': {
        'json':         'pf2e.ancestries',
        'EN_Label':     'Ancestries',
        'DE_Label':     'Abstammungen'
    },
    'ancestryfeatures': {
        'json':         'pf2e.ancestryfeatures',
        'EN_Label':     'Ancestry Features',
        'DE_Label':     'Abstammungsfähigkeiten'
    },
    'archetypes': {
        'json':         'pf2e.archetypes',
        'EN_Label':     'Archetypes',
        'DE_Label':     'Archetypen'
    },
    'backgrounds': {
        'json':         'pf2e.ancestryfeatures',
        'EN_Label':     'Ancestry Features',
        'DE_Label':     'Abstammungsfähigkeiten'
    },
    'bestiary-ability-glossary-srd': {
        'json':         'pf2e.bestiary-ability-glossary-srd',
        'EN_Label':     'Bestiary Ability Glossary',
        'DE_Label':     'Abstammungsfähigkeiten'
    },
    'classes': {
        'json':         'pf2e.classes',
        'EN_Label':     'Classes',
        'DE_Label':     'Klassen'
    },
    'classfeatures': {
        'json':         'pf2e.classfeatures',
        'EN_Label':     'Class Features',
        'DE_Label':     'Klassenmerkmale'
    },
    'conditionitems': {
        'json':         'pf2e.conditionitems',
        'EN_Label':     'Conditions',
        'DE_Label':     'Zustände'
    },
    'consumable-effects': {
        'json':         'pf2e.consumable-effects',
        'EN_Label':     'Consumable Effects',
        'DE_Label':     'Verbrauchsgegenstand-Effekte'
    },
    'equipment': {
        'json':         'pf2e.equipment-srd',
        'EN_Label':     'Equipment',
        'DE_Label':     'Ausrüstung'
    },
    'equipment-effects': {
        'json':         'pf2e.equipment-effects',
        'EN_Label':     'Equipment Effects',
        'DE_Label':     'Ausrüstungseffekte'
    },
#    'feat-effects': {
#        'json':         'pf2e.feat-effects',
#        'EN_Label':     'Feat Effects',
#        'DE_Label':     'Talenteffekte'
#    },
    'feats': {
        'json':         'pf2e.feats-srd',
        'EN_Label':     'Feats',
        'DE_Label':     'Talente'
    },
    'feature-effects': {
        'json':         'pf2e.feature-effects',
        'EN_Label':     'Feature Effects',
        'DE_Label':     'Klassenmerkmaleffekte'
    },
    'spell-effects': {
        'json':         'pf2e.spell-effects',
        'EN_Label':     'Spell Effects',
        'DE_Label':     'Zaubereffekte'
    },
    'spells': {
        'json':         'pf2e.spells-srd',
        'EN_Label':     'Spells',
        'DE_Label':     'Zauber'
    }
}

subprocess.run(['python', 'tools\\xliff-tool.py', '.\\xliff\\system-pf2e.xliff', 'update-from', foundry + 'Data\\systems\\pf2e\\lang\\en.json'])

for c in compendiums:
  print('DB:', c)
  print('json:', compendiums[c]['json'])
  subprocess.run(['python', 'tools\\convert_packs.py', foundry + 'Data\\systems\\pf2e\\packs\\' + c + '.db', 'extract', '-o', 'temp\\' + compendiums[c]['json'] + '.json', '-l', compendiums[c]['EN_Label']])

print('================================')
for c in compendiums:
  print('DB:', c)
  print('json:', compendiums[c]['json'])
  subprocess.run(['python', 'tools\\xliff-tool.py', '.\\xliff\\' + compendiums[c]['json'] + '.xliff', 'update-from', 'temp\\' + compendiums[c]['json'] + '.json'])
