$Path = $PSScriptRoot
$Foundry = "C:\FoundryVTT"
if (!(Test-Path($Foundry))) {
    $Foundry = "E:\FoundryVTT"
}


$compendiums = (
    @{
        name     = 'actions'
        json     = "pf2e.actionspf2e"
        EN_Label = "Actions"
        DE_Label = "Aktionen"
    },
    @{
        name     = 'ancestries'
        json     = "pf2e.ancestries"
        EN_Label = "Ancestries"
        DE_Label = "Abstammungen"
    },
    @{
        name     = 'ancestryfeatures'
        json     = 'pf2e.ancestryfeatures'
        EN_Label = 'Ancestry Features'
        DE_Label = 'Abstammungsfähigkeiten'
    },
    @{
        name     = 'archetypes'
        json     = 'pf2e.archetypes'
        EN_Label = 'Archetypes'
        DE_Label = 'Archetypen'
    },
    @{
        name     = 'backgrounds'
        json     = 'pf2e.backgrounds'
        EN_Label = 'Backgrounds'
        DE_Label = 'Hintergründe'
    },
    @{
        name     = 'bestiary-ability-glossary-srd'
        json     = 'pf2e.bestiary-ability-glossary-srd'
        EN_Label = 'Bestiary Ability Glossary'
        DE_Label = 'Monster-Fähigkeitenglossar'
    },
    @{
        name     = 'bestiary-effects'
        json     = 'pf2e.bestiary-effects'
        EN_Label = 'Bestiary Effects'
        DE_Label = 'Monster-Effekte'
    },
    @{
        name     = 'bestiary-family-ability-glossary'
        json     = 'pf2e.bestiary-family-ability-glossary'
        EN_Label = 'Creature Family Ability Glossary'
        DE_Label = 'Monsterfamilien-Fähigkeitenglossar'
    },
    @{
        name     = 'campaign-effects'
        json     = 'pf2e.campaign-effects'
        EN_Label = 'Campaign Effects'
        DE_Label = 'Kampagneneffekte'
    },
    @{
        name     = 'classfeatures'
        json     = 'pf2e.classfeatures'
        EN_Label = 'Class Features'
        DE_Label = 'Klassenmerkmale'
    },
    @{
        name     = 'conditionitems'
        json     = 'pf2e.conditionitems'
        EN_Label = 'Conditions'
        DE_Label = 'Zustände'
    },
    @{
        name     = 'consumable-effects'
        json     = 'pf2e.consumable-effects'
        EN_Label = 'Consumable Effects'
        DE_Label = 'Verbrauchsgegenstand-Effekte'
    },
    @{
        name     = 'criticaldeck'
        json     = 'pf2e.criticaldeck'
        EN_Label = 'Critical Hit/Fumble Deck'
        DE_Label = 'Kritische Treffer-/Patzer-Karten'
    },
    @{
        name     = 'deities'
        json     = 'pf2e.deities'
        EN_Label = 'Deities'
        DE_Label = 'Gottheiten'
    },
    @{
        name     = 'domains'
        json     = 'pf2e.domains'
        EN_Label = 'Domains'
        DE_Label = 'Domänen'
    },
    @{
        name     = 'equipment'
        json     = 'pf2e.equipment-srd'
        EN_Label = 'Equipment'
        DE_Label = 'Ausrüstung'
    },
    @{
        name     = 'equipment-effects'
        json     = 'pf2e.equipment-effects'
        EN_Label = 'Equipment Effects'
        DE_Label = 'Ausrüstungseffekte'
    },
    @{
        name     = 'familiar-abilities'
        json     = 'pf2e.familiar-abilities'
        EN_Label = 'Familiar Abilities'
        DE_Label = 'Vertrautenfähigkeiten'
    },
    @{
        name     = 'feat-effects'
        json     = 'pf2e.feat-effects'
        EN_Label = 'Feat Effects'
        DE_Label = 'Talenteffekte'
    },
    @{
        name     = 'feats'
        json     = 'pf2e.feats-srd'
        EN_Label = 'Feats'
        DE_Label = 'Talente'
    },
    @{
        name     = 'feature-effects'
        json     = 'pf2e.feature-effects'
        EN_Label = 'Feature Effects'
        DE_Label = 'Klassenmerkmaleffekte'
    },
    @{
        name     = 'hazards'
        json     = 'pf2e.hazards'
        EN_Label = 'Heritages'
        DE_Label = 'Herkünfte'
    },
    @{
        name     = 'heritages'
        json     = 'pf2e.heritages'
        EN_Label = 'Heritages'
        DE_Label = 'Herkünfte'
    },
    @{
        name     = 'spell-effects'
        json     = 'pf2e.spell-effects'
        EN_Label = 'Spell Effects'
        DE_Label = 'Zaubereffekte'
    },
    @{
        name     = 'spells'
        json     = 'pf2e.spells-srd'
        EN_Label = 'Spells'
        DE_Label = 'Zauber'
    }
)

Write-Host "===========================================" >> $Path\update.log
Write-Host "Update XLIFF: system-pf2e.xliff" >> $Path\update.log
python "$Path\xliff-tool.py" "$Path\..\xliff\system-pf2e.xliff" update-from "$Foundry\Data\systems\pf2e\lang\en.json" > $Path\update.log 2>&1
Write-Host "===========================================" >> $Path\update.log
Write-Host "Update XLIFF: system-pf2e-re.xliff" >> $Path\update.log
python "$Path\xliff-tool.py" "$Path\..\xliff\system-pf2e-re.xliff" update-from "$Foundry\Data\systems\pf2e\lang\re-en.json" > $Path\update.log 2>&1

foreach($c in $compendiums) {
    Write-Host "===========================================" >> $Path\update.log
    Write-Host "Convert DB:", $c.name >> $Path\update.log
    python "$Path\convert_packs.py" "$Foundry\Data\systems\pf2e\packs\$($c.name).db" extract -o "$($env:TEMP)\$($c.json).json" -l $c.EN_Label >> $Path\update.log 2>&1
}

Write-Host "==========================================="
foreach($c in $compendiums) {
    Write-Host "===========================================" >> $Path\update.log
    Write-Host "Update XLIFF: $($c.json).xliff" >> $Path\update.log
    if (Test-Path "$Path\..\xliff\$($c.json).xliff") {
        python "$Path\xliff-tool.py" "$Path\..\xliff\$($c.json).xliff" update-from "$($env:TEMP)\$($c.json).json" >> $Path\update.log 2>&1
    } else {
        python "$Path\xliff-tool.py" "$Path\..\xliff\$($c.json).xliff" create -s EN -t DE --source-json "$($env:TEMP)\$($c.json).json" >> $Path\update.log 2>&1
    }
}
